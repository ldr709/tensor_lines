## Compilation

- `git submodule init && git submodule update`
- `autoreconf -i`
- `./configure`
- `make -j8`

## Usage

Go to the `src` directory, then run `../tensor_lines [options] tensor_field10.txt`, or pass whichever other field you want to visualize.
By default, it assumes the file format used by the `tensor_field*.txt` files.
Options:

- `-l`: read the `.lintens3d` file format.
- `-e`: read the `.tetfield` file format.
- `-t`: clip degenerate curves to the tetrahedron.

If it works correctly, the program should extract the degenerate curves and neutral surfaces, and render them.
You can use `WASD` to move the camera horizontally, `Q` and `E` to move it up and down, and `<ESC>` to exit.
To turn the camera, click and drag.

