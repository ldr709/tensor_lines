#version 430

in vec4 pos;

out vec3 tensor0_varying;
out vec2 tensor1_varying;

flat out int instance_id;

uniform mat4 MVP;

// Column x Row
uniform mat4x3 tensor_field0;
uniform mat4x2 tensor_field1;

void main()
{
	// Planar curves have negative w. Draw them on the second instance.
	instance_id = gl_InstanceID;
	bool linear_tensor = instance_id == 0;
	vec4 pos_linear_planar = (linear_tensor ? pos : -pos);

	tensor0_varying = tensor_field0 * pos_linear_planar;
	tensor1_varying = tensor_field1 * pos_linear_planar;

	gl_Position = MVP * pos_linear_planar;
}
