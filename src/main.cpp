#include <config.h>

#include <iostream>
#include <fstream>
#include <functional>
#include <map>
#include <math.h>
#include <memory>
#include <random>
#include <string.h>
#include <utility>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <neutral_tracer/tensor.h>
#include <neutral_tracer/neutral.h>
#include <neutral_tracer/neutral_param.h>
#include <neutral_tracer/gl_utils.h>

#if defined(HAVE_GL_GLUT_H)
#include <GL/glut.h>
#elif defined(HAVE_GLUT_GLUT_H)
#include <GLUT/glut.h>
#else
#error "no GLUT header found."
#endif
//#include <GL/freeglut.h>

const GLfloat mconst_2pi_f = 6.28318530717958647692f;
const GLfloat mconst_pi_f = 3.14159265358979323846f;
const GLfloat mconst_pi_2f = 1.57079632679489661923f;
const GLfloat mconst_atan_1sqrt2f = 0.6154797086703873f;

GLuint vertex_buffer;
GLuint index_buffer;
GLuint tet_vertex_buffer;

bool use_32bit_indices;

GLuint main_shader_program;
GLuint pos_attr;
GLint MVP_uniform;
GLint tensor0_uniform;
GLint tensor1_uniform;

GLuint tet_shader_program;
GLuint tet_pos_attr;
GLint tet_MVP_uniform;

GLuint degenerate_vao;
GLuint tet_vao;

std::vector<degenerate_curve_tracer::vertex> vs;
std::vector<degenerate_curve_tracer::face> faces;
std::vector<degenerate_curve_tracer::tet> tets;

std::unique_ptr<neutral_tracer_param> neutral_surface_tracer;

GLfloat yaw = 0.0f;
GLfloat pitch = mconst_pi_2f;
Vector3gf camera_loc(-0.5, 0.5, 0.5);
GLfloat aspect_ratio = 1.0f;

affine_field<3> field;

int prev_mouse_x;
int prev_mouse_y;

int window_id;

struct vertex_attrs
{
	GLfloat pos[4];
};

static void render_scene();
static Eigen::AngleAxis<GLfloat> view_yaw();

template<typename T, size_t N>
size_t array_size(const T (&array)[N])
{
	return N;
}

static void reshape(int width, int height)
{
	aspect_ratio = (GLfloat) width / height;
	glViewport(0, 0, width, height);
}

static void move_mouse(int x, int y)
{
	int dx = x - prev_mouse_x;
	int dy = y - prev_mouse_y;

	prev_mouse_x = x;
	prev_mouse_y = y;

	GLfloat yaw_diff = dx * mconst_2pi_f / 256;

	yaw = remainder(yaw + yaw_diff, mconst_2pi_f);

	// Subtract because y is down in window coordinates.
	pitch -= dy * mconst_pi_f / 256;

	if (pitch > mconst_pi_f)
		pitch = mconst_pi_f;
	else if (pitch < 0.0f)
		pitch = 0.0f;

	glutPostRedisplay();
}

static void mouse_click(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN)
	{
		prev_mouse_x = x;
		prev_mouse_y = y;
	}
}

static void key_press(unsigned char key, int x, int y)
{
	float move_dist = 0.01f;
	switch (key)
	{
		case 'w':
		case 'W':
			camera_loc -= view_yaw() * (move_dist * Vector3gf::UnitZ());
			break;
		case 's':
		case 'S':
			camera_loc += view_yaw() * (move_dist * Vector3gf::UnitZ());
			break;
		case 'd':
		case 'D':
			camera_loc += view_yaw() * (move_dist * Vector3gf::UnitX());
			break;
		case 'a':
		case 'A':
			camera_loc -= view_yaw() * (move_dist * Vector3gf::UnitX());
			break;
		case 'e':
		case 'E':
			camera_loc[1] += move_dist;
			break;
		case 'q':
		case 'Q':
			camera_loc[1] -= move_dist;
			break;

		case 27:
			glutDestroyWindow(window_id);
			exit(EXIT_SUCCESS);
			return;

		default:
			return;
	}

	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	if (argc < 2 || argc > 4)
	{
		std::cerr << "Usage: " << argv[0]
			<< " [-l] [-e] [-t] tensor_field_file\n";
		return 1;
	}

	if (argc >= 3 && strcmp("-l", argv[1]) && strcmp("-e", argv[1])
	    && strcmp("-t", argv[1]) ||
	    argc >= 4 && strcmp("-l", argv[2]) && strcmp("-e", argv[2])
	    && strcmp("-t", argv[2]))
	{
		std::cerr << "Only supported flags are -l, -e, and -t.\n";
		return 1;
	}

	bool lintens3d = (argc >= 3 && strcmp("-l", argv[1]) == 0 ||
	                  argc >= 4 && strcmp("-l", argv[2]) == 0);
	bool tetfield = (argc >= 3 && strcmp("-e", argv[1]) == 0 ||
	                 argc >= 4 && strcmp("-e", argv[2]) == 0);
	bool only_show_tet = (argc >= 3 && strcmp("-t", argv[1]) == 0 ||
	                      argc >= 4 && strcmp("-t", argv[2]) == 0);

	neutral_tracer::tetrahedron tet_in;
	if (lintens3d)
		create_tensor_field_lintens3d(argv[argc - 1], field);
	else if (tetfield)
	{
		create_tensor_field_tetfield(argv[argc - 1], tet_in);
		field = tet_in.field * tet_in.loc.colwise().homogeneous().inverse();
	}
	else
		create_tensor_field(argv[argc - 1], field);

	// Generate a fake tetrahedron.
	neutral_tracer::tetrahedron tet;
	if (tetfield)
		tet = tet_in;
	else
	{
		tet.loc <<
			1, 0, 0, -1,
			0, 1, 0, -1,
			0, 0, 1, -1;
		tet.field = field * tet.loc.colwise().homogeneous();
	}

	vs.resize(4);
	for (unsigned int i = 0; i < 4; i++)
	{
		vs[i].loc = tet.loc.col(i);
		vs[i].field = tet.field.col(i);
	}

	faces.resize(4);
	for (unsigned int i = 0; i < 4; i++)
		for (unsigned int j = 0; j < 3; j++)
			faces[i].verts[j] = j + (i <= j);

	tets.resize(1);
	for (unsigned int i = 0; i < 4; i++)
	{
		tets[0].verts[i] = i;
		tets[0].faces[i] = i;
	}

	if (only_show_tet)
	{
		degenerate_curve_tracer tracer(vs, faces, tets);
		std::vector<degenerate_curve_tracer::degen_curve>
			degenerate_curves = tracer.trace_curves();

		for (const auto& curve : degenerate_curves)
		{
			size_t prev_index;
			size_t first_index;
			for (auto it = curve.first.begin(); it != curve.first.end(); ++it)
			{
				const degen_point& p = *it;
				size_t index = known_degen_points.size();
				known_degen_points.push_back(p);

				if (it != curve.first.begin())
					known_degen_lines.emplace_back(prev_index, index);
				else
					first_index = index;

				prev_index = index;
			}

			if (curve.second)
				known_degen_lines.emplace_back(prev_index, first_index);
		}
	}
	else
		find_infinite_degenerates(field);

	glutInit(&argc, argv);
	//glutInitContextVersion(4, 3);
	//glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitWindowSize(500, 500);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	window_id = glutCreateWindow("Tensor Lines");

	GLenum err = glewInit();
	if (err != GLEW_OK)
	{
		std::cerr << "Error initializing GLEW: " << glewGetErrorString(err) <<
			std::endl;
		return EXIT_FAILURE;
	}
	if (!GLEW_VERSION_4_3)
	{
		std::cerr << "This application requires OpenGL 4.3" << std::endl;
		return EXIT_FAILURE;
	}

	glutDisplayFunc(render_scene);
	glutReshapeFunc(reshape);
	glutMotionFunc(move_mouse);
	glutMouseFunc(mouse_click);
	glutKeyboardFunc(key_press);

	glGenBuffers(1, &vertex_buffer);
	glGenBuffers(1, &index_buffer);
	glGenBuffers(1, &tet_vertex_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	{
		// Write the data to the buffer.
		glBufferData(GL_ARRAY_BUFFER,
		             known_degen_points.size() * sizeof(vertex_attrs),
		             NULL, GL_STATIC_DRAW);
		vertex_attrs* attrs = (vertex_attrs*)
			glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		for (const auto& p : known_degen_points)
		{
			Eigen::Map<Eigen::Matrix<GLfloat, 4, 1> >(attrs->pos) =
				p.loc.cast<GLfloat>();

			attrs++;
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
	{
		GLushort short_max = (GLushort) -1;
		use_32bit_indices = known_degen_points.size() > (size_t) short_max;
		size_t index_size =
			use_32bit_indices ? sizeof(GLuint) : sizeof(GLushort);

		// Write the data to the buffer.
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		             known_degen_lines.size() * 2 * index_size,
		             NULL, GL_STATIC_DRAW);
		void* attrs = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

		size_t i = 0;
		for (const auto& line_segment : known_degen_lines)
		{
			if (use_32bit_indices)
			{
				((GLuint*) attrs)[i++] = line_segment.first;
				((GLuint*) attrs)[i++] = line_segment.second;
			}
			else
			{
				((GLushort*) attrs)[i++] = line_segment.first;
				((GLushort*) attrs)[i++] = line_segment.second;
			}
		}

		glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, tet_vertex_buffer);
	{
		glBufferData(GL_ARRAY_BUFFER,
		             tets.size() * 6 * 2 * 3 * sizeof(GLfloat),
		             NULL, GL_STATIC_DRAW);
		GLfloat* attrs = (GLfloat*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		for (const auto& tet : tets)
		{
			unsigned int edge[2];
			for (edge[0] = 0; edge[0] < 3; edge[0]++)
			{
				for (edge[1] = edge[0] + 1; edge[1] < 4; edge[1]++)
				{
					for (unsigned int vert = 0; vert < 2; vert++)
					{
						Eigen::Map<Vector3gf> map(attrs);
						map = vs[tet.verts[edge[vert]]].loc.cast<GLfloat>();
						attrs += 3;
					}
				}
			}
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	main_shader_program = create_shader_program("main.vert", "main.frag");

	pos_attr = glGetAttribLocation(main_shader_program, "pos");
	MVP_uniform = glGetUniformLocation(main_shader_program, "MVP");
	tensor0_uniform = glGetUniformLocation(main_shader_program, "tensor_field0");
	tensor1_uniform = glGetUniformLocation(main_shader_program, "tensor_field1");

	tet_shader_program = create_shader_program("tet.vert", "tet.frag");
	tet_pos_attr = glGetAttribLocation(tet_shader_program, "pos");
	tet_MVP_uniform = glGetUniformLocation(tet_shader_program, "MVP");

	glGenVertexArrays(1, &degenerate_vao);
	glGenVertexArrays(1, &tet_vao);

	glBindVertexArray(degenerate_vao);
	glEnableVertexAttribArray(pos_attr);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glVertexAttribPointer(pos_attr, 4, GL_FLOAT, GL_FALSE, sizeof(vertex_attrs),
	                      (void*) offsetof(vertex_attrs, pos));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
	glBindVertexArray(0);

	glBindVertexArray(tet_vao);
	glEnableVertexAttribArray(tet_pos_attr);
	glBindBuffer(GL_ARRAY_BUFFER, tet_vertex_buffer);
	glVertexAttribPointer(tet_pos_attr, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	neutral_surface_tracer.reset(new neutral_tracer_param(
		"neutral_tracer/", vs, faces, tets, 100, 14, 14));

	glutMainLoop();

	neutral_surface_tracer.reset();

	glDeleteVertexArrays(1, &degenerate_vao);

	glDeleteBuffers(1, &vertex_buffer);
	glDeleteBuffers(1, &index_buffer);

	glUseProgram(0);

	glDeleteProgram(main_shader_program);
	glDeleteProgram(tet_shader_program);

	return EXIT_SUCCESS;
}

static Eigen::AngleAxis<GLfloat> view_yaw()
{
	return Eigen::AngleAxis<GLfloat>(-yaw, Vector3gf::UnitY());
}

static Transform3gfa view_mat()
{
	// Build view matrix backwards (because we need the inverse of the camera to
	// world matrix.

	Transform3gfa view_translation(Eigen::Translation<GLfloat, 3>(-camera_loc));

	Transform3gfa view_rotation
		(Eigen::AngleAxis<GLfloat>(-pitch + mconst_pi_2f, Vector3gf::UnitX()) *
		 view_yaw().inverse());

	return view_rotation * view_translation;
}

static Transform3gf projection_mat(GLfloat horizontal_FOV,
                                   GLfloat near, GLfloat far)
{
	// 1/tan(x) = cot(x) = tan(PI/2 - x)
	GLfloat invtan_FOV = tan(mconst_pi_2f - 0.5f * horizontal_FOV);
	GLfloat x_scale = invtan_FOV;
	GLfloat y_scale = x_scale * aspect_ratio;
	GLfloat inv_near_far_dist = 1.0f / (near - far);
	GLfloat z_scale = 2 * far * near * inv_near_far_dist;

	Matrix4gf proj_mat;
	proj_mat << x_scale,       0,                                0,       0,
	                  0, y_scale,                                0,       0,
	                  0,       0, (far + near) * inv_near_far_dist, z_scale,
	                  0,       0,                               -1,       0;

	return Transform3gf(proj_mat);
}

static Transform3gf projection_mat_inf(GLfloat horizontal_FOV,
                                       GLfloat near)
{
	// 1/tan(x) = cot(x) = tan(PI/2 - x)
	GLfloat invtan_FOV = tan(mconst_pi_2f - 0.5f * horizontal_FOV);
	GLfloat x_scale = invtan_FOV;
	GLfloat y_scale = x_scale * aspect_ratio;

	Matrix4gf proj_mat;
	proj_mat << x_scale,       0,  0,       0,
	                  0, y_scale,  0,       0,
	                  0,       0, -1, -2*near,
	                  0,       0, -1,       0;

	return Transform3gf(proj_mat);
}

static void render_degenerate_curves(const Transform3gf& mvp)
{
	glLineWidth(5.0f);

	glUseProgram(main_shader_program);
	glUniformMatrix4fv(MVP_uniform, 1, GL_FALSE, mvp.data());

	Eigen::Matrix<GLfloat, 3, 4>
		tensor_field_glfloat_top = field.cast<GLfloat>().topRows<3>();
	Eigen::Matrix<GLfloat, 2, 4>
		tensor_field_glfloat_bot = field.cast<GLfloat>().bottomRows<2>();
	glUniformMatrix4x3fv(tensor0_uniform, 1, GL_FALSE,
	                     tensor_field_glfloat_top.data());
	glUniformMatrix4x2fv(tensor1_uniform, 1, GL_FALSE,
	                     tensor_field_glfloat_bot.data());

	// Instance 0 draws the linear curves. Instance 1 draws the planar.
	glBindVertexArray(degenerate_vao);
	glDrawElementsInstanced(
		GL_LINES, 2 * known_degen_lines.size(),
		use_32bit_indices ? GL_UNSIGNED_INT : GL_UNSIGNED_SHORT, (void*) 0,
		2);
	glBindVertexArray(0);
}

static void render_tetrahedron(const Transform3gf& mvp)
{
	glLineWidth(3.0f);

	glUseProgram(tet_shader_program);
	glUniformMatrix4fv(tet_MVP_uniform, 1, GL_FALSE, mvp.data());

	glBindVertexArray(tet_vao);
	glDrawArrays(GL_LINES, 0, tets.size() * 6 * 2);
	glBindVertexArray(0);
}

static void setup_lighting(const Transform3gfa& mv)
{
	neutral_tracer_param::light_buf lighting;
	lighting.uKa = 0.4;
	lighting.uKd = 0.4;
	lighting.uKs = 0.6;
	Eigen::Map<Vector4gf>(lighting.uLightPos) << camera_loc, 1.0;
	Eigen::Map<Vector4gf>(lighting.uLightSpecularColor)
		<< 0.4, 0.4, 0.4, 0.0;
	lighting.uShininess = 10.0;
	Eigen::Map<Vector4gf>(lighting.uEyePos) << camera_loc, 1.0;

	neutral_surface_tracer->set_lighting(lighting);
}

static void draw_test_ns()
{
	glBegin(GL_TRIANGLES);
	for (const auto& tri : neutral_surface_tracer->neutral_triangles)
	{
		for (unsigned int i = 0; i < 3; i++)
		{
			Vector3ft loc = neutral_surface_tracer->
				neutral_verts[tri.neutral_v[i]].loc/*.mid*/;
//			if (neutral_surface_tracer->tet_mid_has_wrong_sign(tets[0], loc))
//			if (loc.z() < 0.0)
//				loc = -loc;

			Vector3gf loc_gl = loc.cast<GLfloat>();
			glVertex3fv(loc_gl.data());
		}
	}
	glEnd();
}

static void render_scene()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	Transform3gfa model_view = view_mat();
	Transform3gf model_view_projection =
		projection_mat_inf(mconst_pi_2f, 0.01f) * model_view;

	setup_lighting(model_view);

	render_degenerate_curves(model_view_projection);
	neutral_surface_tracer->render(model_view_projection.matrix(), false);

//	glUseProgram(0);
//	glMatrixMode(GL_PROJECTION);
//	glLoadMatrixf(model_view_projection.data());
//	glMatrixMode(GL_MODELVIEW);

//	glLineWidth(1.0f);

//	// Test neutral surface face intersections.
//	for (const auto& fi : neutral_surface_tracer->face_infos)
//	{
//		size_t i = 0;
//		for (const auto& trace : fi.intersections)
//		{
//			glColor3f(i % 3 == 0, i % 3 == 1, i % 3 == 2);
//			glBegin(GL_LINE_STRIP);
//
//			for (const auto& v_tf : trace)
//			{
//				Vector3gf loc = neutral_surface_tracer->
//					neutral_verts[v_tf.neutral_v].loc.cast<GLfloat>();
//				glVertex3fv(loc.data());
//			}
//
//			glEnd();
//
//			i++;
//		}
//	}

//	// Test neutral surface eigenvector sphere boundaries.
//	for (size_t tet_i = 0; tet_i < neutral_surface_tracer->tets.size(); tet_i++)
//	{
//		const auto& t = neutral_surface_tracer->tets[tet_i];
//		const auto& ti = neutral_surface_tracer->tet_infos[tet_i];
//
//		for (size_t i = 0; i < ti.boundary_ends.size(); i++)
//		{
//			size_t start = i ? ti.boundary_ends[i - 1] : 0;
//			size_t end = ti.boundary_ends[i];
//
//			glColor3f(i % 3 == 0, i % 3 == 1, i % 3 == 2);
//			glBegin(GL_LINE_STRIP);
//
//			const auto first_tetv = ti.tet_verts[start];
//			Vector3ft first_mid;
//			if (first_tetv.neutral_v[1] == (size_t) -1)
//				first_mid = neutral_surface_tracer->
//					neutral_verts[first_tetv.neutral_v[0]].mid;
//			else
//				first_mid = neutral_surface_tracer->
//					neutral_verts[ti.tet_verts[start + 1].neutral_v[0]].mid;
//
//			if (neutral_surface_tracer->tet_mid_has_wrong_sign(t, first_mid))
//				first_mid = -first_mid;
//
//			Vector3ft prev = first_mid;
//			for (size_t j = start; j < end; j++)
//			{
//				Vector3ft mid = neutral_surface_tracer->neutral_verts
//					[ti.tet_verts[j].neutral_v[0]].mid;
//				if (mid.dot(prev) < 0.0)
//					mid = -mid;
//				prev = mid;
//
//				Vector3gf mid_gl = mid.cast<GLfloat>();
//				glVertex3fv(mid_gl.data());
//			}
//
//			Vector3gf first_mid_gl = first_mid.cast<GLfloat>();
//			glVertex3fv(first_mid_gl.data());
//
//			glEnd();
//		}
//	}

//	// Test neutral surface.
//	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//	glEnable(GL_POLYGON_OFFSET_FILL);
//	glPolygonOffset(1.0f, 1.0f);
//	glColor3f(0.0, 1.0, 0.0);
//	draw_test_ns();
//	glDisable(GL_POLYGON_OFFSET_FILL);
//
//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//	glColor3f(1.0, 0.0, 0.0);
//	draw_test_ns();

//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//	glMatrixMode(GL_MODELVIEW);

	render_tetrahedron(model_view_projection);

	glutSwapBuffers();
}
