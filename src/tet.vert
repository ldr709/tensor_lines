#version 430

in vec4 pos;

uniform mat4 MVP;

void main()
{
	gl_Position = MVP * pos;
}
