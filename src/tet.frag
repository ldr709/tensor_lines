#version 430

out vec4 frag_color;

void main()
{
	vec3 rgb = vec3(1, 0, 1);
	frag_color = vec4(rgb, 1.0);
}
