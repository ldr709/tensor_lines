#version 430

in vec3 tensor0_varying;
in vec2 tensor1_varying;

flat in int instance_id;

out vec4 frag_color;

uniform mat4x3 tensor_field0;
uniform mat4x2 tensor_field1;

// tensor*_varying after perspective division.
vec3 tensor0;
vec2 tensor1;

float repeated_eigenvalue()
{
	// Let A be the tensor field sampled at this point.

	// Since this is a degenerate point, there must be a duplicate eigenvalues.
	// y1 = y2 or y2 = y3

	// Let a be the duplicate eigenvalue and b be the other eigenvalue. Then
	// 2*a + b = trace = 0, as the tensor field had it's trace removed when it
	// was loaded. a^2 * b = det(A) because the determinant is the product of
	// the eigenvalues.
	//
	// b = -2*a
	// -2 * a^3 = det(A)
	// sgn(a) = -sgn(det(A))

	// To calculate a use the characteristic polynomial of A, which must equal
	// (y - a)^2 * (y - b) = y^3 - (2*a + b)*y^2 + (a^2 + 2*a*b)*y - (a^2 * b)
	//                     = y^3 - (a^2 + 2*a*b)*y - (a^2 * b)
	// The linear term of the characteristic polynomial of A is
	// (t00*t11 + t00*t22 + t11*t22 - t01^2 - t02^2 - t12^2)*y
	// So this value must equal
	// a^2 + 2*a*b = a^2 - 4*a^2 = -3*a^2
	//
	// This, together with the sign of the determinant, may be used to find a.

	float x_01_sum = tensor0.x + tensor1.x;
	float linear_term =
		tensor0.x * tensor1.x - x_01_sum * x_01_sum -
		tensor0.y * tensor0.y - tensor0.z * tensor0.z - tensor1.y * tensor1.y;

	float determinant =
		tensor0.x * (-tensor1.x * x_01_sum - tensor1.y * tensor1.y) -
		tensor0.z * tensor1.x * tensor0.z +
		tensor0.y * (2 * tensor1.y * tensor0.z + tensor0.y * x_01_sum);

	// -3*a^2 = linear coefficient
	// a = +-sqrt(-linear coefficient / 3)

	return -sign(determinant) * sqrt(-linear_term / 3);
}

// Convert the stacked version of a tensor into a matrix.
mat3 stacked_tensor_to_matrix(vec3 tensor_part0, vec2 tensor_part1)
{
	// The initialization order is column major, but that doesn't matter here as
	// the matrix is symmetric.
	return mat3(tensor_part0.x, tensor_part0.y, tensor_part0.z,
	            tensor_part0.y, tensor_part1.x, tensor_part1.y,
	            tensor_part0.z, tensor_part1.y, -tensor_part0.x - tensor_part1.x);
}

// Get the plane of the eigenvectors that have the repeated eigenvalue. The
// vectors are in the matrix's columns. y is the repeated eigenvalue. This
// function doesn't always return a orthogonal basis, but that shouldn't matter.
mat2x3 get_eigenplane(float y)
{
	// An alternate method would be to calculate the row space of the matrix
	//     A - x*I
	// where x is the nonrepeated eigenvalue.

	// The kernel of this matrix is the eigenplane, if A is the tensor.
	//     m = A - y*I
	// Because y is a repeated eigenvalue, m has rank 3 - 2 = 1. This means
	// that the null space is the same as the null space of one non-zero row
	// (or column, as the matrix is symmetric) of m.

	// At least one of the diagonal elements must be non-zero, so pick the
	// largest (in absolute value) for numerical stability.

	vec3 m0 = tensor0 - vec3(y, 0, 0);
	vec3 m1 = vec3(tensor1, -tensor0.x - tensor1.x) - vec3(y, 0, y);

	vec3 diag_abs = abs(vec3(m0.x, m1.xz));

	// We need to find two vectors that are orthogonal to all of the rows, but
	// that is simplified because all of the rows are scaled versions. This
	// means that I only need to find one nonzero row.

	// Compare all components at once.
	bvec3 comp = greaterThan(diag_abs, diag_abs.yzx);

	mat2x3 basis;
	if (comp.x && !comp.z)
	{
		// Use the first row / column.
		basis[0] = vec3(m0.y, -m0.x,     0);
		basis[1] = vec3(m0.z,     0, -m0.x);
	}
	else if (comp.y && !comp.x)
	{
		// Use the second row / column.
		basis[0] = vec3(-m1.x, m0.y,     0);
		basis[1] = vec3(    0, m1.y, -m1.x);
	}
	else
	{
		// Use the third row / column.
		basis[0] = vec3(-m1.z,     0, m0.z);
		basis[1] = vec3(    0, -m1.z, m1.y);
	}

	return basis;
}

void main()
{
	bool linear_tensor = instance_id == 0;

	// Perspective divide the tensor, as it wasn't done in the vertex shader.
	// gl_FragCoord.w contains 1/w.
	tensor0 = tensor0_varying * gl_FragCoord.w;
	tensor1 = tensor1_varying * gl_FragCoord.w;

	float repeated_value = repeated_eigenvalue();

	mat2x3 eigenplane = get_eigenplane(repeated_value);

	// Remove the affine part of the field.
	mat3 tensor_field0_linear = mat3(tensor_field0);
	mat3x2 tensor_field1_linear = mat3x2(tensor_field1);

	// The tensor field needs to be adjusted to the new coordinate system. First
	// transform it's input coordinates.
	mat2x3 tensor_field0_transformed = tensor_field0_linear * eigenplane;
	mat2x2 tensor_field1_transformed = tensor_field1_linear * eigenplane;

	// These are the tensor field's change with respect to the transformed x and
	// y.
	mat3 tensor_field_x = stacked_tensor_to_matrix
		(tensor_field0_transformed[0], tensor_field1_transformed[0]);
	mat3 tensor_field_y = stacked_tensor_to_matrix
		(tensor_field0_transformed[1], tensor_field1_transformed[1]);

	// Project the tensor field onto the eigenplane, and then remove the z axis.
	mat2 tensor_field_x_proj =
		transpose(eigenplane) * tensor_field_x * eigenplane;
	mat2 tensor_field_y_proj =
		transpose(eigenplane) * tensor_field_y * eigenplane;

	// Wedge if positive.
	float wedge_tri = ((tensor_field_x_proj[1][1] - tensor_field_x_proj[0][0]) *
	                   tensor_field_y_proj[0][1] -
	                   (tensor_field_y_proj[1][1] - tensor_field_y_proj[0][0]) *
	                   tensor_field_x_proj[0][1]);
	vec3 rgb;
	float hue;
	if (wedge_tri > 0)
		if (linear_tensor)
			rgb = vec3(0, 1, 0); // Linear wedge: green
		else
			rgb = vec3(1, 1, 0); // Planar wedge: yellow
	else
		if (linear_tensor)
			rgb = vec3(0, 0, 1); // Linear trisector: blue
		else
			rgb = vec3(1, 0, 0); // Planar trisector: red

	frag_color = vec4(rgb, 1.0);
}
